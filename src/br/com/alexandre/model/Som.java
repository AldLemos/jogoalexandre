package br.com.alexandre.model;

import java.applet.Applet;
import java.applet.AudioClip;
import java.net.URL;

public class Som {
	URL urlPainel, urlFase1;
	AudioClip musicaPainel, musicaFase1;
	
	public Som() {
		urlPainel = getClass().getResource("painel.wav");
		urlFase1 = getClass().getResource("fase1.wav");
		
		musicaPainel = Applet.newAudioClip(urlPainel);
		musicaFase1 = Applet.newAudioClip(urlFase1);
	}

	public AudioClip getMusicaPainel() {
		return musicaPainel;
	}

	public AudioClip getMusicaFase1() {
		return musicaFase1;
	}
}
