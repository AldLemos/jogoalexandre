package br.com.alexandre.model;

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;

public abstract class Elemento {
	static final int COLIDINDO_ACIMA = 0;
	static final int COLIDINDO_DIR = 1;
	static final int COLIDINDO_ABAIXO = 2;
	static final int COLIDINDO_ESQ = 3;	
	protected Rectangle2D.Double posicao;
	private int altura,largura;
	protected Elemento[] colisaoElementos;
	private boolean ativo;
	private boolean colhido;
	
	public Elemento(int x, int y, int largura, int altura) {
		super();
		posicao = new Rectangle2D.Double(x, y, 1, 1);
		this.largura = largura;
		this.altura = altura;
		colisaoElementos = new Elemento[4];
	}
	public Elemento(int x, int y) {
		super();
		posicao = new Rectangle2D.Double(x, y, 1, 1);
		colisaoElementos = new Elemento[4];
	}
	
	public abstract void tick();
	public abstract void draw(Graphics g);
	
	public void novoY() {
		posicao.y = 130;
	}
	
	public void pego(int x, int y) {
		posicao.x = x;
		posicao.y = y;
	}
	
	public void liberarElemento(double x, double y) {
		posicao.x = x;
		posicao.y = y;
	}
	
	public int getAltura() {
		return altura;
	}
	public int getLargura() {
		return largura;
	}
	public void setAltura(int altura) {
		this.altura = altura;
	}
	public void setLargura(int largura) {
		this.largura = largura;
	}
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	
	public Elemento[] getColisaoElementos() {
		return colisaoElementos;
	}
	public boolean isColhido() {
		return colhido;
	}
	public void setColhido(boolean colhido) {
		this.colhido = colhido;
	}
	public Rectangle2D.Double getPosicao() {
		return posicao;
	}
	public void setPosicao(Rectangle2D.Double posicao) {
		this.posicao = posicao;
	}
}