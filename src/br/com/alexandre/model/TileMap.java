package br.com.alexandre.model;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;

public class TileMap {
	private final int numColuna = 33;
	private final int numLinha = 16;
	private final int numColTile = 16;
	private final int tamTile = 32;

	private int camada[][];

	private int mapaLargura = numColuna * tamTile;
	private int mapaAltura = numLinha * tamTile;

	private BufferedImage tileSet;
	private BufferedImage mapa = new BufferedImage(mapaLargura, mapaAltura, BufferedImage.TYPE_4BYTE_ABGR);
	private Graphics2D dbg = mapa.createGraphics();

	public TileMap(String nomeTileSet, String nomeMapaMatriz) {
		try {
			tileSet = ImageIO.read(getClass().getClassLoader().getResourceAsStream(nomeTileSet));
		} catch (IOException e) {
			System.out.println("N�o leu tileSet");
			e.printStackTrace();
		}

		camada = carregaMatriz(nomeMapaMatriz);
	}

	public void montarMapa(){
		mapa.createGraphics();

		int tile;
		int tileLinha, tileColuna;

		for(int i = 0; i < numLinha; i++ ){
			for(int j = 0; j < numColuna; j++){
				tile = (camada[i][j] != 0) ? (camada[i][j] - 1) : 0;
				tileLinha = (tile / numColTile) | 0;
				tileColuna = (tile % numColTile) | 0;
				dbg.drawImage(tileSet, (j * tamTile), (i * tamTile), (j * tamTile) + tamTile,
						(i * tamTile) + tamTile, (tileColuna * tamTile), (tileLinha * tamTile),
						(tileColuna * tamTile) + tamTile, (tileLinha * tamTile) + tamTile, null);
			}
		}
	}

	public int[][] carregaMatriz(String nomeMapa){
		int[][] matriz = new int[numLinha][numColuna];

		InputStream input = getClass().getClassLoader().getResourceAsStream(nomeMapa);
		BufferedReader bufferedR = new BufferedReader(new InputStreamReader(input));

		ArrayList<String> linhas = new ArrayList<String>();
		String linha = "";

		try {
			while((linha = bufferedR.readLine())!=null)
				linhas.add(linha);

			int j = 0;
			for(int i = 0; i < linhas.size(); i++){
				StringTokenizer token = new StringTokenizer(linhas.get(i), ",");

				while (token.hasMoreElements()) {
					matriz[i][j] = Integer.parseInt(token.nextToken());
					j++;
				}
				j = 0;
			}

		} catch (IOException e) {
			System.out.println("Erro ao carregar matriz");
			e.printStackTrace();
		}
		return matriz;
	}

	public int[][] getCamada() {
		return camada;
	}

	public void setCamada(int[][] camada) {
		this.camada = camada;
	}

	public BufferedImage getTileSet() {
		return tileSet;
	}

	public void setTileSet(BufferedImage tileSet) {
		this.tileSet = tileSet;
	}

	public BufferedImage getMapa() {
		return mapa;
	}

	public void setMapa(BufferedImage mapa) {
		this.mapa = mapa;
	}

	public int getNumColuna() {
		return numColuna;
	}

	public int getNumLinha() {
		return numLinha;
	}

	public int getNumColTile() {
		return numColTile;
	}

	public int getTamTile() {
		return tamTile;
	}
}