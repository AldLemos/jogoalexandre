package br.com.alexandre.model;

public class Tempo extends Thread{
	private int tempo;
	
	public Tempo() {
		tempo = 200;
	}
	public void run() {
		for(;;) {			
			try {
				sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			tempo -= 1;
		}
	}
	public int getTempo() {
		return tempo;
	}
	public void setTempo(int tempo) {
		this.tempo = tempo;
	}
	
}