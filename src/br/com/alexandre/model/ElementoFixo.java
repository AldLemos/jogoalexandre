package br.com.alexandre.model;

import java.awt.Graphics;

public class ElementoFixo extends Elemento{
	
	public ElementoFixo(int x, int y, int largura, int altura) {
		super(x, y, largura, altura);
		posicao.setRect(x, y, largura, altura);
	}
	
	@Override
	public void tick() {}
	
	@Override
	public void draw(Graphics g) {
//		g.drawRect((int) posicao.x, (int) posicao.y, getLargura(), getAltura());
	}
}