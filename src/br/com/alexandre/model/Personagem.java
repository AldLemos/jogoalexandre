package br.com.alexandre.model;

import java.awt.Color;
import java.awt.Graphics;
import java.io.IOException;

public class Personagem extends ElementoSprite{
	private int tecla;
	private int saude;
	private int saudeY, saudeAltura;
	private int index;
	private boolean chave;
	
	public Personagem(int aparencia, int columns, int rows, int x, int y, double valorValocidade, String link, int saude)
			throws IOException {
		super(aparencia, columns, rows, x, y, valorValocidade, link);
		tecla = 0;
		this.saude = saude;
		saudeY = 298;
		saudeAltura = 200;
		index = 0;
		chave = false;
	}

	@Override
	public void tick() {
		switch (tecla) {
		case 1:
			aceleracao.x = 4;
			break;
		case 2:
			aceleracao.x = -4;
			break;
		case 3:
			if(colisaoElementos[COLIDINDO_ABAIXO]!= null)
			aceleracao.y = -10;
			break;
		case 4:
			if(colisaoElementos[COLIDINDO_ABAIXO]!= null && posicao.y <= 400) {
				colisaoElementos[COLIDINDO_ABAIXO].setAtivo(false);
				colisaoElementos[COLIDINDO_ABAIXO].setColhido(true);
			}
				break;
		}
		
		super.tick();
		super.mudaAparencia();
		
		if(colisaoElementos[COLIDINDO_ACIMA] != null){
			posicao.y = colisaoElementos[COLIDINDO_ACIMA].posicao.y + colisaoElementos[COLIDINDO_ACIMA].posicao.height;
			atualizaSaude(1);
		}
	}
	
	@Override
	public void draw(Graphics g) {
		g.drawImage(sprites[getAparencia()], (int) posicao.x, (int) posicao.y, null);
		g.setColor(Color.GREEN);
		g.fillRect(973, saudeY, 69, saudeAltura);
	}

	public void atualizaSaude(int porcentagem) {
		double n = (212 / 100) * porcentagem;
		saude -= porcentagem;
		saudeY += n;
		saudeAltura -= n;
	}
	
	public boolean colideAbaixo() {
		if(colisaoElementos[COLIDINDO_ABAIXO]!= null)
			return true;
		return false;
	}
	
	public void setTecla(int tecla) {
		this.tecla = tecla;
	}

	public int getSaude() {
		return saude;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public boolean isChave() {
		return chave;
	}

	public void setChave(boolean chave) {
		this.chave = chave;
	}

	public void setSaude(int saude) {
		this.saude = saude;
	}
}