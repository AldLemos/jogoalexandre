package br.com.alexandre.model;

import java.awt.Graphics;
import java.io.IOException;

public class Item extends ElementoSprite {
	
	public Item(int aparencia, int columns, int rows, int x, int y, double valorValocidade, String link)
			throws IOException {
		super(aparencia, columns, rows, x, y, valorValocidade, link);
		setAtivo(false);
		setColhido(false);
	}

	public void tick() {
		if(isAtivo()) {
			super.tick();	
		}
	}

	public void draw(Graphics g) {
		g.drawImage(sprites[getAparencia()], (int) posicao.x, (int) posicao.y, null);
	}
}
