package br.com.alexandre.model;

import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public abstract class ElementoSprite extends Elemento{
	BufferedImage spriteSheet;
	int rows, columns;
	private int aparencia;
	BufferedImage[] sprites;
	Point2D.Double velocidade;
	Point2D.Double aceleracao;
	Point2D.Double velMax;
	double resistencia;
	double gravidade;
	
	public ElementoSprite(int aparencia, int columns, int rows, int x, int y, double valorValocidade, String link) throws IOException {
		super(x, y);
		this.aparencia	= aparencia;
		this.columns	= columns;
		this.rows		= rows;
		spriteSheet		= ImageIO.read(new File(link));
		velocidade		= new Point2D.Double(valorValocidade, valorValocidade);
		aceleracao		= new Point2D.Double();
		velMax			= new Point2D.Double(5, 15);
		resistencia		= 0.53;
		gravidade		= 0.45;
		posicao.width	= spriteSheet.getWidth()/columns;
		posicao.height	= spriteSheet.getHeight()/rows;
		
		sprites = new BufferedImage[columns * rows];
		for(int i = 0; i < columns; i++) {
			for(int j = 0; j < rows; j++) {
				sprites[(i * rows) + j] = spriteSheet.getSubimage(i * (int) posicao.width, j * (int) posicao.height, (int) posicao.width, (int) posicao.height);
			}
		}		
	}

	public ElementoSprite(int x, int y, double valorValocidade) {
		super(x, y);
		velocidade = new Point2D.Double(valorValocidade, valorValocidade);
		aceleracao = new Point2D.Double();
		velMax = new Point2D.Double(5, 15);
		resistencia = 0.53;
		gravidade = 0.45;
	}

	@Override
	public void tick() {
		velocidade.x += aceleracao.x;

		if(velocidade.x < - velMax.x){
			velocidade.x = -velMax.x;
		} else if(velocidade.x > velMax.x){
			velocidade.x = velMax.x;
		}

		aceleracao.y += gravidade;

		velocidade.y += aceleracao.y;
		if(velocidade.y < - velMax.y){
			velocidade.y = -velMax.y;
		} else if(velocidade.y > velMax.y){
			velocidade.y = velMax.y;
		}

		if(velocidade.y < 0){
			if(colisaoElementos[COLIDINDO_ACIMA] != null){
				posicao.y = colisaoElementos[COLIDINDO_ACIMA].posicao.y + colisaoElementos[COLIDINDO_ACIMA].posicao.height;
				velocidade.y = 0;
				aceleracao.y = 0;
			}
		} else if(velocidade.y > 0){
			if(colisaoElementos[COLIDINDO_ABAIXO] != null && colisaoElementos[COLIDINDO_ABAIXO].posicao.x < 980){
				posicao.y = colisaoElementos[COLIDINDO_ABAIXO].posicao.y - posicao.height + 1;
				velocidade.y = 0;
				aceleracao.y = 0;
			}
		}

		if(velocidade.x < 0){
			if(colisaoElementos[COLIDINDO_ESQ] != null){
				posicao.x = colisaoElementos[COLIDINDO_ESQ].posicao.x + colisaoElementos[COLIDINDO_ESQ].posicao.width -1;
				velocidade.x = 0;
				aceleracao.x = 0;
			} else{
				velocidade.x += resistencia;
				if(velocidade.x > 0){
					velocidade.x = 0;
				}
			}
		} else if(velocidade.x > 0){
			if(colisaoElementos[COLIDINDO_DIR] != null){
				posicao.x = colisaoElementos[COLIDINDO_DIR].posicao.x - posicao.width +1;
				velocidade.x = 0;
				aceleracao.x = 0;
			} else{
				velocidade.x -= resistencia;
				if(velocidade.x < 0){
					velocidade.x = 0;
				}
			}
		}

		posicao.x += velocidade.x;
		posicao.y += velocidade.y;
		aceleracao.x = 0;
		aceleracao.y = 0;
	}
	
	public void mudaAparencia() {
		if(velocidade.x < 0 && colisaoElementos[COLIDINDO_ABAIXO]!= null) {
			switch (aparencia) {
			case 1:
				aparencia = 5;
				break;
			case 5:
				aparencia = 9;
				break;
			case 9:
				aparencia = 13;
				break;
			case 13:
				aparencia = 1;
				break;
			}
		}
		if(velocidade.x > 0 && colisaoElementos[COLIDINDO_ABAIXO]!= null) {
			switch (aparencia) {
			case 2:
				aparencia = 6;
				break;
			case 6:
				aparencia = 10;
				break;
			case 10:
				aparencia = 14;
				break;
			case 14:
				aparencia = 2;
				break;
			}
		}
	}

	public void setAparencia(int aparencia) {
		this.aparencia = aparencia;
	}

	public int getAparencia() {
		return aparencia;
	}
}