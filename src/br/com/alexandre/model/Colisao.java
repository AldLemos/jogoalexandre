package br.com.alexandre.model;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class Colisao {
	ArrayList<Elemento> elementos;
	static int index;

	public Colisao(ArrayList<Elemento> elementos) {
		this.elementos = elementos;
	}
	
	public void atualizaArray(ArrayList<Elemento> elementos) {
		this.elementos = elementos;
	}
	
	public void atualiza(){		
		for (Elemento obj : elementos) {
			for(int i = 0; i < 4; i++){
				obj.colisaoElementos[i] = null;
			}
		}
		
		for(int i1 = 0; i1 < elementos.size() - 1; i1++){
			Elemento obj1 = elementos.get(i1);
			for(int i2 = i1; i2 < elementos.size(); i2++){
				Elemento obj2 = elementos.get(i2);
					if(obj1.posicao.intersects(obj2.posicao)){
						Rectangle2D rect = obj1.posicao.createIntersection(obj2.posicao);
							if(rect.getWidth() > rect.getHeight()){
								if(obj1.posicao.getCenterY() < obj2.posicao.getCenterY()){
									obj1.colisaoElementos[Elemento.COLIDINDO_ABAIXO] = obj2;
									obj2.colisaoElementos[Elemento.COLIDINDO_ACIMA] = obj1;
								} else{
									obj1.colisaoElementos[Elemento.COLIDINDO_ACIMA] = obj2;
									obj2.colisaoElementos[Elemento.COLIDINDO_ABAIXO] = obj1;
								}
							} else{
								if(obj1.posicao.getCenterX() < obj2.posicao.getCenterX()){
									obj1.colisaoElementos[Elemento.COLIDINDO_DIR] = obj2;
									obj2.colisaoElementos[Elemento.COLIDINDO_ESQ] = obj1;
								} else{
									obj1.colisaoElementos[Elemento.COLIDINDO_ESQ] = obj2;
									obj2.colisaoElementos[Elemento.COLIDINDO_DIR] = obj1;
								}
							}
					}
			}
		}
	}
	
}