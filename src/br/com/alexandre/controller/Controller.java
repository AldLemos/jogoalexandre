package br.com.alexandre.controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import br.com.alexandre.model.Som;
import br.com.alexandre.model.Tempo;
import br.com.alexandre.view.Fase1;
import br.com.alexandre.view.Fase2;
import br.com.alexandre.view.Janela;
import br.com.alexandre.view.Mensagem;
import br.com.alexandre.view.Painel;

public class Controller extends Thread implements ActionListener{
	private Tempo tempo;
	private Janela janela;
	private Painel painelJanela;
	private Fase1 fase1;
	private Fase2 fase2;
	private Som som;
	
	public Controller(Tempo tempo, Janela janela, Painel painelJanela, Fase1 fase1, Fase2 fase2, Som som) {
		this.tempo = tempo;
		this.janela = janela;
		this.painelJanela = painelJanela;
		this.fase1 = fase1;
		this.fase2 = fase2;
		this.som = som;
		
		this.janela.add(painelJanela);
		this.janela.repaint();
		this.painelJanela.requestFocusInWindow();
		this.som.getMusicaPainel().loop();
	}	
	
	public void control() {
		painelJanela.getProxButton().addActionListener(this);
		painelJanela.getSairButton().addActionListener(this);
		painelJanela.getIniciarButton().addActionListener(this);
		janela.addKeyListener(new KeyHandler());
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		if(event.getSource()==painelJanela.getProxButton()) {
			switch (painelJanela.getCount()) {
			case 0:
				painelJanela.setCount(painelJanela.getCount()+1);
				painelJanela.getPersonagem().setIcon(painelJanela.getLabelsIcon().get(painelJanela.getCount()));
				break;
			case 1:
				painelJanela.setCount(painelJanela.getCount()+1);
				painelJanela.getPersonagem().setIcon(painelJanela.getLabelsIcon().get(painelJanela.getCount()));
				break;
			case 2:
				painelJanela.setCount(painelJanela.getCount()+1);
				painelJanela.getPersonagem().setIcon(painelJanela.getLabelsIcon().get(painelJanela.getCount()));
				break;
			case 3:
				painelJanela.setCount(0);
				painelJanela.getPersonagem().setIcon(painelJanela.getLabelsIcon().get(painelJanela.getCount()));
				break;
			}
			janela.repaint();
		}
		if(event.getSource()==painelJanela.getIniciarButton()) {
			janela.remove(painelJanela);
			janela.add(fase1);
			janela.repaint();
			fase1.setAtivo(true);
			fase1.getBlocos().start();
			fase1.iniciaPersonagem(painelJanela.getPers()[painelJanela.getCount()]);
			tempo.start();
			this.start();
			som.getMusicaPainel().stop();
			som.getMusicaFase1().loop();
		}
		
		if(event.getSource()==painelJanela.getSairButton()) {
			if(Mensagem.confirmaSair())
				System.exit(0);
		}
		janela.requestFocus();
	}
	
	public class KeyHandler implements KeyListener {
		
		public void keyPressed(KeyEvent event) {
			if(event.getKeyCode()==KeyEvent.VK_ESCAPE){
				if(Mensagem.confirmaSair())
					System.exit(0);
			}
			if(event.getKeyCode()==KeyEvent.VK_D) {
				fase1.getPlayer1().setAparencia(2);
				fase1.getPlayer1().setTecla(1);
			}
			if(event.getKeyCode()==KeyEvent.VK_A) {
				fase1.getPlayer1().setAparencia(1);
				fase1.getPlayer1().setTecla(2);
			}
			if(event.getKeyCode()==KeyEvent.VK_W) {
				fase1.getPlayer1().setAparencia(0);
				fase1.getPlayer1().setTecla(3);
			}
			if(event.getKeyCode()==KeyEvent.VK_P) {
				fase1.getPlayer1().setTecla(4);
			}
			if(event.getKeyCode()==KeyEvent.VK_L) {
				fase1.largarCaixa();
			}
		}

		public void keyReleased(KeyEvent arg0) {
			fase1.getPlayer1().setTecla(0);
		}

		public void keyTyped(KeyEvent arg0) {}	
	}

	public void run() {
		while(fase1.isAtivo()) {
			fase1.setTempo(tempo.getTempo());
			if(fase1.isConcluido()) {
				fase1.setRunning(false);
				fase1.setAtivo(false);
				fase1.getBlocos().stop();
				tempo.stop();
				if(Mensagem.fase1Completada()) {
					tempo.setTempo(200);
					janela.remove(fase1);
					janela.add(fase2);
					janela.repaint();
					fase2.iniciaPersonagem(painelJanela.getPers()[painelJanela.getCount()]);
					fase2.getPlayer1().setSaude(fase1.getPlayer1().getSaude());
					fase1 = null;
					System.gc();
					Mensagem.fase2incompleta();
					System.exit(0);
				} else {
					System.exit(0);
				}
			}
			if(fase1.getPlayer1().getSaude() <= 0) {
				fase1.setTempo(0);
				tempo.stop();
				fase1.getBlocos().stop();
			}
			if(fase1.getTempo() <= 0) {
				fase1.setRunning(false);
				fase1.setAtivo(false);
				Mensagem.gameOver();
				System.exit(0);
			}
		}
	}
}