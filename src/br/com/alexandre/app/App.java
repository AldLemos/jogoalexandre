package br.com.alexandre.app;

import javax.swing.JFrame;

import br.com.alexandre.controller.Controller;
import br.com.alexandre.model.Som;
import br.com.alexandre.model.Tempo;
import br.com.alexandre.view.Fase1;
import br.com.alexandre.view.Fase2;
import br.com.alexandre.view.Janela;
import br.com.alexandre.view.Painel;

public class App extends JFrame{
	
	public static void main(String[] args) {
		Tempo tempo = new Tempo();
		Janela janela = new Janela();
		Painel painelJanela = new Painel();
		Fase1 fase1 = new Fase1(Janela.LARGURA, Janela.ALTURA);
		Fase2 fase2 = new Fase2(Janela.LARGURA, Janela.ALTURA);
		Som som = new Som();
		Controller controller = new Controller(tempo, janela, painelJanela, fase1, fase2, som);
		controller.control();
	}
}