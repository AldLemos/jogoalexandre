package br.com.alexandre.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.imageio.ImageIO;

import br.com.alexandre.model.Colisao;
import br.com.alexandre.model.Elemento;
import br.com.alexandre.model.ElementoFixo;
import br.com.alexandre.model.Item;
import br.com.alexandre.model.Personagem;
import br.com.alexandre.model.TileMap;

public class Fase2 extends Jogo{
	private BufferedImage background;
	private TileMap terreno;
	private TileMap inventario;
	private Colisao colisao;
	private ArrayList<Elemento> elementos;
	private Personagem player1;
	private boolean ativo = false;
	private boolean concluido = false;
	private Blocos blocos;
	private Item caixa;
	private Item chave, cadeado;
	private int tempo;
	private Font font;
	private String info;

	public Fase2(int Largura, int Altura) {
		super(Largura, Altura);
		elementos = new ArrayList<Elemento>();
		colisao = new Colisao(elementos);
		terreno = new TileMap("tileset.png", "terreno2.txt");
		inventario = new TileMap("tileset.png", "inventario.txt");

		try {
			background = ImageIO.read(new File("resource/background2.png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		blocos = new Blocos();
		iniciaCaixa();
		iniciaElementos();
		try {
			chave = new Item(0, 1, 1, 33, 46, 0, "resource/chave.png");
			cadeado = new Item(0, 1, 1, 880, 46, 0, "resource/cadeado.png");
			elementos.add(chave);
			elementos.add(cadeado);
		} catch (IOException e) {
			e.printStackTrace();
		}
		font = new Font("Arial", Font.BOLD, 20);
		info = "";
		tempo = 0;
	}
	
	@Override
	public void gameUpdate() {
		inventario.montarMapa();
		terreno.montarMapa();
		for (Elemento e : elementos) {
			e.tick();
			pegarChave(chave, player1);
			objetivo(cadeado, player1);
			if(e.isColhido()) {
				e.pego(990, 150);
			}
		}
		colisao.atualiza();
	}

	@Override
	public void gameRender() {
		g.drawImage(background, 0, 0, null);
		g.drawImage(inventario.getMapa(), 0, 0, null);
		g.drawImage(terreno.getMapa(), 0, 0, null);
		g.setFont(font);
		g.drawString(Integer.toString(tempo), 990, 105);
		g.setColor(Color.BLACK);
		g.drawString(info, 400, 32);		
		
		for (Elemento e : elementos) {
			e.draw(g);
		}
		
	}
	
	public void iniciaElementos() {
		int x = 0;
		int y = 0;
		for(int i = 0; i<terreno.getNumLinha(); i++) {
			for(int j = 0; j<terreno.getNumColuna(); j++){
				if(terreno.getCamada()[i][j] != 0) {
				elementos.add(new ElementoFixo(x, y, terreno.getTamTile()-1, terreno.getTamTile()-1));
				}
				x += terreno.getTamTile();
			}
			x = 0;
			y += terreno.getTamTile();
		}
	}
	
	public void iniciaPersonagem(String nome) {
		try {
			player1 = new Personagem(0, 4, 4, 500, 250, 10, "resource/"+nome+".png", 100);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		elementos.add(player1);
	}

	public void iniciaCaixa() {
		int x = 45;
		int y = -500;
		for(int i = 0; i < 5; i++) {
			for (int j = 0; j < 22; j++) {
				try {
					caixa = new Item(0, 1, 1, x, y, 0, "resource/bloco.png");
					elementos.add(caixa);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				x += 40;
			}
			x = 45;
			y += 34;
		}
	}
	
	public void largarCaixa() {
		for(int i = 0; i < elementos.size(); i++) {
			if(elementos.get(i).isColhido() && player1.colideAbaixo()) {
				elementos.get(i).liberarElemento(player1.getPosicao().x, player1.getPosicao().y - 40);
				elementos.get(i).setColhido(false);
				elementos.get(i).setAtivo(true);
			}
		}
	}
	
	public void pegarChave(Elemento item1, Elemento player) {
		if(player.getPosicao().intersects(item1.getPosicao())) {
			item1.pego(990, 200);
			player1.setChave(true);
		}
	}
	
	public void objetivo(Elemento item1, Elemento player) {
		if(player.getPosicao().intersects(item1.getPosicao()) && player1.isChave()) {
			concluido = true;
		} else if(player.getPosicao().intersects(item1.getPosicao()) && !player1.isChave()) {
			info = "Voc� precisa de uma chave!";
		} else {
			info = "";
		}
	}
	
	public Personagem getPlayer1() {
		return player1;
	}

	public void setPlayer1(Personagem player1) {
		this.player1 = player1;
	}

	public ArrayList<Elemento> getElementos() {
		return elementos;
	}

	public void setElementos(ArrayList<Elemento> elementos) {
		this.elementos = elementos;
	}
	

	public void setTempo(int tempo) {
		this.tempo = tempo;
	}

	public int getTempo() {
		return tempo;
	}

	public boolean isConcluido() {
		return concluido;
	}

	public void setConcluido(boolean concluido) {
		this.concluido = concluido;
	}
	
	public Blocos getBlocos() {
		return blocos;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}


	public boolean isAtivo() {
		return ativo;
	}
	
	public class Blocos extends Thread {
		private ArrayList<Integer> numero;
		
		public Blocos () {
			numero = new ArrayList<>();
		}
		
		public void run() {
			for(int i = 0; i < 140; i ++) {
				numero.add(i);
			}
			Collections.shuffle(numero);
			while (ativo) {
				for(int i = 0; i < 140; i++) {
					elementos.get(numero.get(i)).novoY();
					elementos.get(numero.get(i)).setAtivo(true);
					try {
						Thread.sleep(1500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				ativo = false;
				stop();
			}
		}
		
	}
}
