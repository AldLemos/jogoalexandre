package br.com.alexandre.view;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Painel extends JPanel {
	private BufferedImage backgroud0;
	private JLabel personagem;
	private ImageIcon hoody, jeff, red, jane;
	private ArrayList<ImageIcon> labelsIcon;
	private JButton proxButton, iniciarButton, sairButton;
	private int count;
	private String[] pers = {"hoody", "jeff", "red", "jane"};
	protected int tempo;
	
	public Painel() {
		setLayout(null);
		count = 0;
		labelsIcon = new ArrayList<>();
		proxButton = new JButton("<< >>");
		proxButton.setBounds(493, 330, 100, 20);
		iniciarButton = new JButton("Iniciar");
		iniciarButton.setBounds(493, 370, 100, 20);
		sairButton = new JButton("Sair");
		sairButton.setBounds(493, 410, 100, 20);
		setSize(Janela.LARGURA, Janela.ALTURA);
		
		hoody = new ImageIcon(getClass().getResource("/selecionar/hoody.png")); labelsIcon.add(hoody);
		jeff  = new ImageIcon(getClass().getResource("/selecionar/jeff.png"));  labelsIcon.add(jeff);
		red   = new ImageIcon(getClass().getResource("/selecionar/red.png"));   labelsIcon.add(red);
		jane  = new ImageIcon(getClass().getResource("/selecionar/jane.png"));  labelsIcon.add(jane);
		personagem = new JLabel(labelsIcon.get(count));
		personagem.setBounds(493, 160, 100, 170);
		add(personagem);
		
		add(proxButton);
		add(iniciarButton);
		add(sairButton);
		
		
		try {
			backgroud0 = ImageIO.read(new File("resource/background0.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		tempo = 0;
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(backgroud0, 0, 0, null);
	}

	public JButton getIniciarButton() {
		return iniciarButton;
	}

	public JButton getSairButton() {
		return sairButton;
	}

	public ArrayList<ImageIcon> getLabelsIcon() {
		return labelsIcon;
	}

	public void setLabelsIcon(ArrayList<ImageIcon> labelsIcon) {
		this.labelsIcon = labelsIcon;
	}

	public JButton getProxButton() {
		return proxButton;
	}

	public void setProxButton(JButton proxButton) {
		this.proxButton = proxButton;
	}

	public JLabel getPersonagem() {
		return personagem;
	}

	public void setPersonagem(JLabel personagem) {
		this.personagem = personagem;
	}
	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String[] getPers() {
		return pers;
	}
}