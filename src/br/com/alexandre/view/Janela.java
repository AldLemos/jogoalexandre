package br.com.alexandre.view;

import javax.swing.JFrame;

public class Janela extends JFrame{
	public static final int LARGURA = 1056;
	public static final int ALTURA = 512;
	
	public Janela() {
		super("Jogo MPOO");
		setSize(LARGURA, ALTURA);
		setUndecorated(true);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		
		setVisible(true);
	}
}