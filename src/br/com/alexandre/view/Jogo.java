package br.com.alexandre.view;

import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public abstract class Jogo extends JPanel implements Runnable {

	public int  largura = 680;
	public int  altura = 380;
	private Thread thread;
	private boolean running;
	private BufferedImage image;
	protected Graphics2D g;
	private int FPS = 30;

	@SuppressWarnings("unused")
	private Double averageFPS;

	public Jogo(int Largura,int Altura) {
		super();
		this.largura = Largura;
		this.altura = Altura;
		setPreferredSize(new Dimension(largura, altura));
		setSize(largura, altura);
		setFocusable(true);
		requestFocus();
	
	}

	// Fun�oes
	public void addNotify() {
		super.addNotify();

		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}

	public void run() {
		running = true;

		image = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
		g = (Graphics2D) image.getGraphics();

		long startTime;
		long URDTimeMillis;
		long waitTime;
		long totalTime = 0;

		int frameCount = 0;
		int maxFrameCount = 30;

		long tragetTime = 1000 / FPS;
		
		while (running) {

			startTime = System.nanoTime();
			
			gameUpdate();
			gameRender();
			gameDraw();
			
			URDTimeMillis = (System.nanoTime() - startTime) / 1000000;
			waitTime = tragetTime - URDTimeMillis;

			try {
				Thread.sleep(waitTime);
			} catch (Exception e) {
			}

			totalTime += System.nanoTime() - startTime;
			frameCount++;

			if (frameCount == maxFrameCount) {
				averageFPS = 1000.0 / ((totalTime / frameCount) / 1000000);
				frameCount = 0;
				totalTime = 0;
			}
		}

		gameDraw();
	}

	public abstract void gameUpdate();
	public abstract void gameRender();

	

	private void gameDraw() {
		Graphics2D g2 = (Graphics2D) this.getGraphics();
		g2.drawImage(image, 0, 0, null);
		g2.dispose();
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

}