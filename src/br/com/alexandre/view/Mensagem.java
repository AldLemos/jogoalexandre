package br.com.alexandre.view;

import javax.swing.JOptionPane;

public class Mensagem {
	public static boolean confirmaSair(){
		if(JOptionPane.showConfirmDialog(null, "Tem certeza que deseja sair?") == JOptionPane.YES_OPTION){
			return true;
		}
		return false;
	}
	
	public static boolean fase1Completada(){
		if(JOptionPane.showConfirmDialog(null, "PARAB�NS!\nDeseja ir para Fase 2?") == JOptionPane.YES_OPTION){
			return true;
		}
		return false;
	}
	
	public static void faltaChave() {
		JOptionPane.showMessageDialog(null, "� preciso ter uma chave no seu invent�rio!");
	}
	
	public static void gameOver() {
		JOptionPane.showMessageDialog(null, "GAME OVER!");
	}
	
	public static void fase2incompleta() {
		JOptionPane.showMessageDialog(null, ":(\nA Fase 2 ainda est� sendo desenvolvida.\nNos pr�ximos dias estar� tudo pronto!\n :)");
	}
}